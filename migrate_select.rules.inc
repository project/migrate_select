<?php

/**
 * @file
 * Rules integration for migrate_select module.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function migrate_select_rules_action_info() {
  $actions = array();

  $actions['migrate_select_entity_needs_update_rules_action'] = array(
    'label' => t('Select entity for migration'),
    'parameter' => array(
      'entity' => array(
        'type' => 'entity',
        'label' => t('Entity'),
        'description' => t('The entity to mark for migration.'),
      ),
    ),
    'group' => t('Migrate'),
  );

  return $actions;
}

/**
 * Rules action for marking an entity as 'needs_update' for migration.
 *
 * @param object $wrapper
 *   An entity wrapper for entity which should be selected for migration.
 *
 * @see migrate_select_rules_action_info()
 */
function migrate_select_entity_needs_update_rules_action(EntityDrupalWrapper $wrapper) {
  $entity = $wrapper->value();
  // Emulate $context array which VBO passes to action callbacks
  // when processing entities using Batch API.
  $context = array(
    'entity_type' => $wrapper->type(),
    'progress' => array(
      'current' => 1,
      'total' => 1,
    ),
  );
  migrate_select_entity_needs_update_action($entity, $context);
}

/**
 * @}
 */
